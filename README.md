# Web App
> Phone connection establishment web application.

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Screenshots](#screenshots)
* [Local Setup](#local-setup)
* [Ansible Setup](#ansible-setup)

<a name="general-information"></a>
## General Information
> Web App it's an application that allows you to make a phone call between agent number and client number.<br/>
> The project was done partially during classes and partially independently at home.

<a name="technologies-used"></a>
## Technologies Used
- **npm** - version 8.5.0
- **Node.js** - version 16.14.2
- **Vue.js** - version 3.2.33
- **Socket.io** - version 4.5.0
- **Express** - version 4.18.1
- **Ansible** - version 2.9.6
- **nginx** - version 1.18.0

<a name="features"></a>
## Features
- Phone connection establishment
- Ringing phone animation on the home page (StartView.vue)
- Calling animation on the ringing page (RingingView.vue)

<a name="screenshots"></a>
## Screenshots
![Screenshot 1](img/screenshot1.png)
![Screenshot 2](img/screenshot2.png)
![Screenshot 3](img/screenshot3.png)

<a name="local-setup"></a>
## Local Setup
1. Clone repository
	* Windows: ```git clone https://Verq_99@bitbucket.org/verq_99/webapp.git```<br/>
	* Linux: ```git clone git@bitbucket.org:verq_99/webapp.git```
2. Install Node.js and npm: ```https://nodejs.org/en/download/```
3. Open terminal in project folder (webapp)
4. Install all necessary tools
	* Express: ```npm install --save express```
	* Vue.js:
		```
		npm i @vue/cli-service
		cd front
		npm install @babel/core @babel/preset-env
		cd ..
		```
	* Socket.io:
		```
		npm install socket.io
		cd front
		npm install socket.io-client
		cd ..
		```
5. Insert your data into configuration files:
	* config.js:
		```
		module.exports = {
    		dialer: {
        	url: 'https://uni-call.fcc-online.pl',
        	login: '<your_login>',
        	password: '<your_password>'
    		},
    		agent_number: <your_number>,
    		api: {
        	prefix: '/api',
        	url: '<backend_url>',
   			},
    		front: {
        	url: '<front_url>'
    		}
		}
		```
	* front/src/main.ts
		```
		...
		const socket = io('<backend_url>', {
    		reconnection: false,
    		transports: ["websocket", "polling"]
		});
		```
6. Run the following commands:
```
node index.js
cd front
npm run serve
```
7. Open url of frontend in your browser

<a name="ansible-setup"></a>
## Ansible Setup (Linux)
> You need to have 2 machines (2 servers) with Linux operating system.<br/>
> We will use Azure Virtual Machines for this purpose [(portal.azure.com)](https://portal.azure.com/#home). Remember to download your SSH key and set it for both VM's.

### Server 1 Setup
1. Move your downloaded SSH key to your home directory
2. Open terminal and run the following commands:
	```
	eval `ssh-agent -s`
	ssh-add ~/.ssh/ssh.pem
	ssh ubuntu@<server1-ip>
	```
3. Install necessary packages on the server:
	```
	url -sL https://deb.nodesource.com/setup_16.x | sudo bash -
	sudo apt-get update
	sudo apt-get install -y nodejs nginx
	sudo apt install -y ansible
	sudo npm install -g pm2
	```
4. Modify nginx configuration file running ```sudo nano -n /etc/nginx/nginx.conf``` and simply replace it with the following code:
	```
	user www-data;
	worker_processes auto;
	pid /run/nginx.pid;
	include /etc/nginx/modules-enabled/*.conf;

	events {
			worker_connections 768;
	}

	http {
			sendfile on;
			tcp_nopush on;
			tcp_nodelay on;
			keepalive_timeout 65;
			types_hash_max_size 2048;
			server_tokens off;

			include /etc/nginx/mime.types;
			default_type application/octet-stream;
			access_log /var/log/nginx/access.log;
			error_log /var/log/nginx/error.log;
			gzip on;
			server {
			listen 80;
				root /var/www/webapp/front/dist;
				index index.html;

			location /api {
					proxy_pass http://localhost:3000;
			}
			location /api/socket {
					proxy_set_header Upgrade $http_upgrade;
					proxy_set_header Connection "upgrade";
					proxy_set_header Host $http_host;
					proxy_set_header X-Real-IP $remote_addr;
					proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

					proxy_set_header X-Frame-Options SAMEORIGIN;
					proxy_pass http://localhost:3000;
			}
		}
	}
	```
5. Restart nginx server: ```sudo systemctl restart nginx```
6. Copy SSH key from your local machine to Server 1
	```
	exit
	scp ~/.ssh/ssh.pem ubuntu@<server1-ip>:~/.ssh/key.pem
	```
7. Add SSH key on Server 1
	```
	sh ubuntu@<adres-ip-servera>
	eval "$(ssh-agent -s)"
	ssh-add ~/.ssh/key.pem
	```
8. Open new terminal and log into the Server 2:
	```
	eval `ssh-agent -s`
	ssh-add ~/.ssh/ssh.pem
	ssh ubuntu@<server2-ip>
	```
### Usage
- Make sure that both servers are turned on
- Log into the Server 1: ```ssh ubuntu@<server1-ip>```
- Clone git repository: ```git clone git@bitbucket.org:verq_99/webapp.git```
- ```cd webapp/ansible```
- Run ansible playbook: ```ansible-playbook -K -e "target=<server2-ip> login=<your-login> password=<your-password> number=<your-number>" -i inventory install.yml```
- Insert ip address of Server 2 into your browser